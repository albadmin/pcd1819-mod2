package pcd2018.tictactoe;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

class Score {
  int wonX = 0;
  int wonO = 0;

  Score(int wonX, int wonO) {
    this.wonO = wonO;
    this.wonX = wonX;
  }

  static Score score(Game game) {
    if (game.status() == GameStatus.PLAYER_O_WON) {
      return Score.WON_O;
    } else if (game.status() == GameStatus.PLAYER_X_WON) {
      return Score.WON_X;
    } else {
      return Score.ONGOING;
    }
  }

  static Score sum(Score a, Score b) {
    return new Score(a.wonX + b.wonX, a.wonO + b.wonO);
  }

  static Score ONGOING = new Score(0, 0);
  static Score WON_X = new Score(1, 0);
  static Score WON_O = new Score(0, 1);
}

public class IteratorCounter {
  public static void main(String[] args) {
    GameIterator iterator = new GameIterator(Game.newGame());
    long start = System.currentTimeMillis();
    Score res = StreamSupport.stream(iterator, true)
        .collect(Collectors.reducing(new Score(0, 0), Score::score, Score::sum));
    long end = System.currentTimeMillis();
    System.out.println("Won O: " + res.wonO + " Won X:" + res.wonX + " (" + (end - start) + ")");
  }
}
