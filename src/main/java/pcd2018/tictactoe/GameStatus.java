package pcd2018.tictactoe;

/**
 * Possible statuses of a game *
 */
public enum GameStatus {
  ONGOING, PLAYER_O_WON, PLAYER_X_WON;
}
