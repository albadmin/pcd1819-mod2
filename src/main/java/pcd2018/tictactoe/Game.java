package pcd2018.tictactoe;

import io.vavr.collection.List;
import io.vavr.control.Option;

/**
 * Game status and methods to progress.
 *
 */
public class Game {

  /**
   * Bitmasked game status:
   * 
   * 0-15 player 1 moves 16-31 player 2 moves 32-39 numer of moves so far
   * 
   */
  final long game;
  final GameStatus status;

  /**
   * Bitmasks for the game cells.
   */
  final List<Long> masks = List.of(0x1L, 0x2L, 0x4L, 0x8L, 0x10L, 0x20L, 0x40L, 0x80L, 0x100L);
  /**
   * Winning combinations.
   */
  final List<Long> wins = List.ofAll(0x1c0L, 0x38L, 0x7L, 0x124L, 0x92L, 0x49L, 0x111L, 0x54L);

  final long CURRENT_PLAYER_MASK = 0x100000000L;
  final long MOVES_MASK = 0xff00000000L;
  final long PLAYER_O_MASK = 0xffffL;
  final long PLAYER_X_MASK = 0xffff0000L;
  final long NCELLS = 9;

  private Game(long game, GameStatus status) {
    this.game = game;
    this.status = status;
  }

  /**
   * 
   * @return a representation of the actual status
   */
  public String gameString() {
    long playerO = game & PLAYER_O_MASK;
    long playerX = (game & PLAYER_X_MASK) >> 16;
    return masks.grouped(3).foldLeft(new StringBuffer(), (buf, row) -> {
      row.forEach((l) -> {
        if ((playerO & l) != 0) {
          buf.append("O");
        } else if ((playerX & l) != 0) {
          buf.append("X");
        } else {
          buf.append(" ");
        }
      });
      return buf.append("\n");
    }).toString();
  }

  /**
   * Perform a move
   * 
   * @param move the cell where the current player moves
   * @return the game after the move
   */
  public Option<Game> move(long move) {
    long mv = 1 << move;
    long board = (game & PLAYER_O_MASK) | ((game & PLAYER_X_MASK) >> 16);
    boolean playerO = (game & CURRENT_PLAYER_MASK) == 0;
    return List.of(mv).filter((Long m) -> (m & board) == 0)
        .map((m) -> (game | (playerO ? m : m << 16)) + CURRENT_PLAYER_MASK).map((Long m) -> new Game(m, status(m)))
        .headOption();
  }

  public GameStatus status() {
    return status;
  }

  /**
   * Current number of moves played
   * 
   * @return
   */
  public long index() {
    return (game & MOVES_MASK) >> 32;
  }

  /**
   * 
   * @return list of games with all the possible legal moves applied
   */
  public List<Game> moves() {
    List<Game> result = List.empty();
    if (status.equals(GameStatus.ONGOING)) {
      long board = (game & PLAYER_O_MASK) | ((game & PLAYER_X_MASK) >> 16);
      boolean playerO = (game & CURRENT_PLAYER_MASK) == 0;
      result = masks.filter((m) -> (m & board) == 0).map((m) -> (game | (playerO ? m : m << 16)) + CURRENT_PLAYER_MASK)
          .map((Long m) -> new Game(m, status(m)));
    }
    return result;
  }

  GameStatus status(long game) {
    GameStatus result;
    long playerO = (game & PLAYER_O_MASK);
    long playerX = (game & PLAYER_X_MASK) >> 16;
    if (wins.exists((l) -> ((playerO & l) == l)))
      result = GameStatus.PLAYER_O_WON;
    else if (wins.exists((l) -> ((playerX & l) == l)))
      result = GameStatus.PLAYER_X_WON;
    else
      result = GameStatus.ONGOING;
    return result;
  }

  /**
   * Builder for an empty game
   * 
   * @return An empty, initialized game.
   */
  public static Game newGame() {
    return new Game(0L, GameStatus.ONGOING);
  }

}
