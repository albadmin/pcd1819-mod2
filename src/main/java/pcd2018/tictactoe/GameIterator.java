package pcd2018.tictactoe;

import java.util.Spliterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;

public class GameIterator implements Spliterator<Game> {

  LinkedBlockingQueue<Game> current;
  LinkedBlockingQueue<Game> next;

  public GameIterator(Game seed) {
    current = new LinkedBlockingQueue<Game>(seed.moves().toJavaList());
    next = new LinkedBlockingQueue<Game>();
  }

  @Override
  public boolean tryAdvance(Consumer<? super Game> action) {
    boolean result = false;
    if (current.isEmpty() && next.isEmpty()) {
      // nothing more available
      result = false;
    } else if (current.isEmpty()) {
      // more elements from the seed
      Game res = next.remove();
      next.addAll(res.moves().toJavaList());
      action.accept(res);
      result = true;
    } else {
      // a ready element
      Game res = current.remove();
      next.addAll(res.moves().toJavaList());
      action.accept(res);
      result = true;
    }
    return result;
  }

  @Override
  public Spliterator<Game> trySplit() {
    Spliterator<Game> res = null;
    if (current.size() > 1) {
      List<Game> dest = List.empty();
      int len = current.size() / 2;
      for (int i = 0; i <= len; i++)
        dest = dest.append(current.remove());
      res = new GameIterator(dest.iterator());
    }
    return res;
  }

  private GameIterator(Iterator<Game> current) {
    this.current = new LinkedBlockingQueue<Game>(current.flatMap((g) -> g.moves().toJavaList()).toJavaList());
    this.next = new LinkedBlockingQueue<Game>();
  }

  @Override
  public long estimateSize() {
    return -1;
  }

  @Override
  public int characteristics() {
    return Spliterator.IMMUTABLE | Spliterator.DISTINCT | Spliterator.NONNULL;
  }

}
