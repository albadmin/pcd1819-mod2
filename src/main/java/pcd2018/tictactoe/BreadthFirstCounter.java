package pcd2018.tictactoe;

import io.vavr.collection.List;

public class BreadthFirstCounter {

  public static void main(String[] args) {
    Game root = Game.newGame();
    long start = System.currentTimeMillis();
    List<Game> first = root.moves();
    // System.out.println("first: " + first.length());

    List<Game> second = first.flatMap(g -> g.moves());
    // System.out.println("second: " + second.length());

    List<Game> third = second.flatMap(g -> g.moves());
    // System.out.println("third: " + third.length());

    List<Game> fourth = third.flatMap(g -> g.moves());
    // System.out.println("fourth: " + fourth.length());

    List<Game> fifth = fourth.flatMap(g -> g.moves());
    int wonO = fifth.count(g -> g.status() == GameStatus.PLAYER_O_WON);
    int wonX = fifth.count(g -> g.status() == GameStatus.PLAYER_X_WON);
    List<Game> fifthOn = fifth.filter(g -> g.status() == GameStatus.ONGOING);
    // System.out.println("fifth: " + fifthOn.length() + " won O/X " + wonO + " " + wonX);

    List<Game> sixth = fifthOn.flatMap(g -> g.moves());
    wonO += sixth.count(g -> g.status() == GameStatus.PLAYER_O_WON);
    wonX += sixth.count(g -> g.status() == GameStatus.PLAYER_X_WON);
    List<Game> sixthOn = sixth.filter(g -> g.status() == GameStatus.ONGOING);
    // System.out.println("sixth: " + sixthOn.length() + " won O/X " + wonO + " " + wonX);

    List<Game> seventh = sixthOn.flatMap(g -> g.moves());
    wonO += seventh.count(g -> g.status() == GameStatus.PLAYER_O_WON);
    wonX += seventh.count(g -> g.status() == GameStatus.PLAYER_X_WON);
    List<Game> seventhOn = seventh.filter(g -> g.status() == GameStatus.ONGOING);
    // System.out.println("seventh: " + seventhOn.length() + " won O/X " + wonO + " " + wonX);

    List<Game> eighth = seventhOn.flatMap(g -> g.moves());
    wonO += eighth.count(g -> g.status() == GameStatus.PLAYER_O_WON);
    wonX += eighth.count(g -> g.status() == GameStatus.PLAYER_X_WON);
    List<Game> eighthOn = eighth.filter(g -> g.status() == GameStatus.ONGOING);
    // System.out.println("eighth: " + eighthOn.length() + " won O/X " + wonO + " " + wonX);

    List<Game> ninth = eighthOn.flatMap(g -> g.moves());
    wonO += ninth.count(g -> g.status() == GameStatus.PLAYER_O_WON);
    wonX += ninth.count(g -> g.status() == GameStatus.PLAYER_X_WON);
    List<Game> ninthOn = ninth.filter(g -> g.status() == GameStatus.ONGOING);

    long end = System.currentTimeMillis();
    System.out.println("ninth: " + ninthOn.length() + " won O/X " + wonO + " " + wonX + " (" + (end - start) + ")");
  }

}
