package pcd2018.lab2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import pcd2018.lab2.bowling.Bowler;

class BowlerSender implements Runnable {

  private Random random = new Random();
  private DatagramSocket socket;
  private int lane, level;

  BowlerSender(DatagramSocket socket, int lane, int level) {
    this.socket = socket;
    this.lane = lane;
    this.level = level;
  }

  @Override
  public void run() {
    byte[] buf = new byte[16];
    try {
      DatagramPacket packet = new DatagramPacket(buf, 16, InetAddress.getByName("localhost"), BowlingServer.GAME_PORT);
      Bowler bowler = new Bowler(level);
      for (Integer pin : bowler.pins()) {
        String message = lane + ":" + pin;
        System.out.println(message);
        // formatta il messaggio
        packet.setData(message.getBytes());
        // invia il messaggio
        socket.send(packet);
        // attendi qualche centinaio di millisecondi
        Thread.sleep(random.nextInt(1000));
      }
    } catch (UnknownHostException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

}

/**
 * Sends to the server data about single throws in the form "<lane>:<score> via datagrams.
 * 
 * Should accept a single param with the number of the lane, and wait a little between datagrams.
 * 
 * Run with: ./gradlew bowlingClient --args=4
 * 
 * Try with different numbers as lane.
 * 
 */
public class BowlingClient {
  public static void main(String[] args) throws InterruptedException, IOException {
    // usando la comunicazione via Datagrams
    // prepara un pacchetto per inviare i messaggi
    DatagramSocket socket = new DatagramSocket();
    Random random = new Random();
    ExecutorService service = Executors.newCachedThreadPool();
    IntStream.range(0, 10).mapToObj((i) -> new BowlerSender(socket, i, random.nextInt(10)))
        .forEach(t -> service.execute(t));

    Thread.sleep(5000);
    service.shutdown();
    do {
      service.awaitTermination(5, TimeUnit.SECONDS);
      System.out.println("Awating termination...");
    } while (!service.isTerminated());
    System.out.println("Done.");

    byte[] buf = new byte[16];
    DatagramPacket packet = new DatagramPacket(buf, 16, InetAddress.getByName("localhost"), BowlingServer.GAME_PORT);
    packet.setData("end".getBytes());
    socket.send(packet);
  }
}
