package pcd2018.lab2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import io.vavr.control.Option;
import pcd2018.lab2.bowling.BowlingScorer;

/**
 * Single result read from the network, that should be added to the score.
 */
class Result {
  final Integer lane;
  final Integer pins;

  Result(int lane, int pins) {
    this.lane = lane;
    this.pins = pins;
  }
}

/**
 * Reads results from the queue and mantains a map of active games.
 * 
 * Can init new games when needed. Prints out status of every modified game.
 */
class ScorePrinter implements Runnable {

  private BlockingQueue<Result> inputQueue;
  Map<Integer, BowlingScorer> games = new HashMap<Integer, BowlingScorer>();

  ScorePrinter(BlockingQueue<Result> inputQueue) {
    this.inputQueue = inputQueue;
  }

  @Override
  public void run() {
    // finché non veniamo interrotti:
    // prendi un risultato dalla coda

    try {
      while (true) {
        Result result = inputQueue.take();
        BowlingScorer game;
        if (games.containsKey(result.lane)) {
          // aggiungilo alla partita cui appartiene
          game = games.get(result.lane).pin(result.pins);
        } else {
          // crea la partita se non esiste
          game = new BowlingScorer().pin(result.pins);
        }
        System.out.println("Lane " + result.lane + " status " + game.status() + " score " + game.score());
        games.put(result.lane, game);
      }
    } catch (InterruptedException e) {
      // quando veniamo interrotti:
      System.out.println("--------------------");
      // stampa tutte le partite osservate
      for (Integer lane : games.keySet()) {
        System.out
            .println("Lane: " + lane + " status: " + games.get(lane).status() + " score: " + games.get(lane).score());
      }
    }
  }
}

/**
 * Reads a packet from network as "<lane>:<score>" and puts in queue to be summed up.
 */
public class BowlingServer {

  /**
   * Port where to listen to incoming messages, and where clients should send them.
   */
  static final int GAME_PORT = 56423;

  public static void main(String args[]) throws IOException, InterruptedException {
    boolean done = false;
    Option<Thread> score = Option.none();
    try (
        // con un socket per Datagram opportunamente predisposto:
        DatagramSocket socket = new DatagramSocket(GAME_PORT);) {
      // prepara un pacchetto per ricevere i messaggi
      byte[] buf = new byte[16];
      DatagramPacket packet = new DatagramPacket(buf, 16);
      // istanzia ed avvia il sommatore
      BlockingQueue<Result> queue = new LinkedBlockingQueue<Result>();
      score = Option.of(new Thread(new ScorePrinter(queue)));
      score.get().start();
      // ripeti:
      while (!done) {
        // ricevi un messaggio
        // System.out.println("Receiving");
        socket.receive(packet);
        // interpretalo ed ottieni un risultato da sommare
        String input = new String(packet.getData(), 0, packet.getLength());
        if (input.equals("end"))
          done = true;
        else {
          String[] res = input.split(":");
          // System.out.println("Received " + res[0] + ":" + res[1]);
          Result result = new Result(Integer.parseInt(res[0]), Integer.parseInt(res[1]));
          // inoltralo al sommatore
          queue.put(result);
        }
      }
    } finally {
      score.forEach((t) -> t.interrupt());
    }
  }
}
