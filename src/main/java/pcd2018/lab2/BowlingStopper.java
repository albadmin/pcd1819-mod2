package pcd2018.lab2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Sends to the server the "end" message
 */
public class BowlingStopper {
  public static void main(String[] args) throws InterruptedException, IOException {
    // usando la comunicazione via Datagrams
    // prepara un pacchetto per inviare i messaggi
    byte[] buf = new byte[16];
    DatagramPacket packet = new DatagramPacket(buf, 16, InetAddress.getByName("localhost"), BowlingServer.GAME_PORT);
    DatagramSocket socket = new DatagramSocket();
    String message = "end";
    packet.setData(message.getBytes());
    // invia il messaggio
    socket.send(packet);
    // attendi qualche centinaio di millisecondi
    socket.close();
  }
}
