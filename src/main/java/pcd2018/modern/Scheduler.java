package pcd2018.modern;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class Scheduler {
  static final boolean[] done = new boolean[] { false };

  static public void main(String[] args) throws InterruptedException {
    System.out.println("Defining...");
    Observable.range(0, 1000000).map(new RxDivisors()).filter(new RxPerfectPredicate())
        .subscribeOn(Schedulers.computation()).subscribe((c) -> {
          System.out.println(c);
        }, (t) -> {
          t.printStackTrace();
        }, () -> {
          System.out.println("Done");
          done[0] = true;
        });
    System.out.println("Defined");
    while (!done[0])
      Thread.sleep(1000);
    System.out.println("End");
  }
}