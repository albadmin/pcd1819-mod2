package pcd2018.modern;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class Parallel {
  static final boolean[] done = new boolean[] { false };

  static public void main(String[] args) throws InterruptedException {
    System.out.println("Defining...");
    Flowable.range(0, 1000000).parallel(4).runOn(Schedulers.computation()).map(new RxDivisors())
        .filter(new RxPerfectPredicate()).sequential().subscribe((c) -> {
          System.out.println(c);
        }, (t) -> {
          t.printStackTrace();
        }, () -> {
          System.out.println("Done");
          done[0] = true;
        });
    System.out.println("Defined");
    while (!done[0])
      Thread.sleep(1000);
    System.out.println("End");
  }
}