package pcd2018.tictactoe;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

import org.junit.jupiter.api.Test;

import io.vavr.collection.List;

public class GameTest {

  List<String> firstMoveList = List.of("O  \n   \n   \n", " O \n   \n   \n", "  O\n   \n   \n", "   \nO  \n   \n",
      "   \n O \n   \n", "   \n  O\n   \n", "   \n   \nO  \n", "   \n   \n O \n", "   \n   \n  O\n");
  List<String> secondMoveList = List.of("OX \n   \n   \n", "O X\n   \n   \n", "O  \nX  \n   \n", "O  \n X \n   \n",
      "O  \n  X\n   \n", "O  \n   \nX  \n", "O  \n   \n X \n", "O  \n   \n  X\n");
  List<String> thirdMoveList = List.of("OXO\n   \n   \n", "OX \nO  \n   \n", "OX \n O \n   \n", "OX \n  O\n   \n",
      "OX \n   \nO  \n", "OX \n   \n O \n", "OX \n   \n  O\n");

  @Test
  public void moves() {
    Game game = Game.newGame();
    assertEquals("   \n   \n   \n", game.gameString());
    List<Game> moves = game.moves();
    assertEquals(9, moves.size());
    assertIterableEquals(firstMoveList, moves.map((Game g) -> g.gameString()));
    moves.forEach((g) -> assertEquals(1, g.index()));
    moves.forEach((g) -> assertEquals(GameStatus.ONGOING, g.status()));
  }

  @Test
  public void secondMove() {
    Game game = Game.newGame();
    assertEquals("   \n   \n   \n", game.gameString());
    List<Game> moves = game.moves().head().moves();
    assertEquals(8, moves.size());
    assertIterableEquals(secondMoveList, moves.map((Game g) -> g.gameString()));
    moves.forEach((g) -> assertEquals(2, g.index()));
    moves.forEach((g) -> assertEquals(GameStatus.ONGOING, g.status()));
  }

  @Test
  public void thirdMove() {
    Game game = Game.newGame();
    assertEquals("   \n   \n   \n", game.gameString());
    List<Game> moves = game.moves().head().moves().head().moves();
    assertEquals(7, moves.size());
    assertIterableEquals(thirdMoveList, moves.map((Game g) -> g.gameString()));
    moves.forEach((g) -> assertEquals(3, g.index()));
    moves.forEach((g) -> assertEquals(GameStatus.ONGOING, g.status()));
  }

  @Test
  public void wonGame() {
    Game game = List.of(0, 1, 3, 2, 6).foldLeft(Game.newGame(), (gm, move) -> {
      return gm.move(move).get();
    });
    assertEquals(GameStatus.PLAYER_O_WON, game.status());
    assertEquals("OXX\nO  \nO  \n", game.gameString());
  }

}
